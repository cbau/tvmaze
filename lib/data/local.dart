import 'package:flutter/foundation.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';

class Local {
  static final _instance = Local._();

  final showStore = intMapStoreFactory.store('shows');

  Database? _database;

  /// Private constructor for this class.
  Local._();

  /// Factory instance constructor.
  factory Local() => _instance;

  Future<Database> get database async =>
      _database ??= await (kIsWeb ? databaseFactoryWeb : databaseFactoryIo)
          .openDatabase('appdb.db');
}
