import 'package:flutter/foundation.dart' as base;

class Log {
  static void debugPrint(String? message, {int? wrapWidth}) {
    if (base.kDebugMode) {
      base.debugPrint(message, wrapWidth: wrapWidth);
    }
  }

  static void print(String? message, {int? wrapWidth}) {
    base.debugPrint(message, wrapWidth: wrapWidth);
  }
}
