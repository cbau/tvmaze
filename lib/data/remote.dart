import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'package:tvmaze/data/log.dart';

class Remote {
  static final _instance = Remote._();

  bool useLocalDemo = false;

  /// The API server URL the app points to.
  String get server => 'https://api.tvmaze.com';

  /// Gets the search people API.
  String get apiSearchPeople => '$server/search/people';

  /// Gets the search shows API.
  String get apiSearchShows => '$server/search/shows';

  /// Gets the shows API.
  String get apiShows => '$server/shows';

  /// Gets the show episodes API.
  String get apiShowEpisodes => '$server/shows/:showId/episodes';

  /// Gets the people cast credits API.
  String get apiPeopleCastCredits => '$server/people/:personId/castcredits';

  /// Private constructor for this class.
  Remote._();

  /// Factory instance constructor.
  factory Remote() => _instance;

  /// Gets the remote string from the [path].
  Future<String> getRemoteString({
    required String method,
    required String path,
    Map<String, String>? headers,
    dynamic body,
  }) async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw Exception('Internet connection not found.');
    }

    // Create request.
    Log.debugPrint('Request: $method $path');
    final request = http.Request(method, Uri.parse(path));

    // Set headers.
    if (headers != null) {
      Log.debugPrint('Headers: ${json.encode(headers)}');
      request.headers.addAll(headers);
    }

    // Set body.
    if (body != null) {
      Log.debugPrint('Body: ${json.encode(body)}');
      if (body is Map<String, String>) {
        request.bodyFields = body;
      } else if (body is List<int>) {
        request.bodyBytes = body;
      } else {
        request.body = body.toString();
      }
    }

    // Get response.
    final response = await request.send();
    final data = await response.stream.bytesToString();

    // Get data from response.
    final statusCode = response.statusCode;
    if (statusCode < 300) {
      Log.debugPrint('Response: ($statusCode) $data');
      return data;
    } else {
      final message = '$statusCode ${response.reasonPhrase}';
      Log.debugPrint('Response: $message');
      throw Exception(message);
    }
  }
}
