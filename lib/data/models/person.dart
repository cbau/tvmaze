import 'package:tvmaze/data/models/image_data.dart';

class Person {
  final int id;
  final ImageData image;
  final String name;

  const Person({
    this.id = 0,
    this.image = const ImageData(),
    this.name = '',
  });

  Person.fromMap(Map<String, dynamic>? map)
      : this(
          id: map?['id'] as int? ?? 0,
          image: ImageData.fromMap(map?['image'] as Map<String, dynamic>?),
          name: map?['name'] as String? ?? '',
        );

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{
      'id': id,
      'image': image.toMap(),
      'name': name,
    };

    return map;
  }
}
