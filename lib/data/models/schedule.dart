class Schedule {
  final List<String> days;
  final String time;

  const Schedule({
    this.days = const [],
    this.time = '',
  });

  Schedule.fromMap(Map<String, dynamic>? map)
      : this(
          days: (map?['days'] as List?)?.cast() ?? [],
          time: map?['time'] as String? ?? '',
        );

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{
      'days': days,
      'time': time,
    };

    return map;
  }
}
