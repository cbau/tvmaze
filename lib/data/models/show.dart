import 'package:sembast/sembast.dart';
import 'package:tvmaze/data/local.dart';
import 'package:tvmaze/data/models/image_data.dart';
import 'package:tvmaze/data/models/schedule.dart';
import 'package:tvmaze/data/repositories/show_repository.dart';

class Show {
  final int id;
  final ImageData image;
  final List<String> genres;
  final Schedule schedule;
  final String name;
  final String summary;
  var _isFavorite = false;
  String? _scheduleFormatted;

  Show({
    this.id = 0,
    this.image = const ImageData(),
    bool isFavorite = false,
    this.genres = const [],
    this.name = '',
    this.schedule = const Schedule(),
    String? scheduleFormatted,
    this.summary = '',
  }) {
    if (isFavorite) {
      _isFavorite = true;
    } else {
      _getFavoriteFromLocal();
    }
    _scheduleFormatted = scheduleFormatted;
  }

  Show.fromMap(Map<String, dynamic>? map)
      : this(
          id: map?['id'] as int? ?? 0,
          image: map?['imageUrl'] is String
              ? ImageData(medium: map!['imageUrl'] as String)
              : ImageData.fromMap(map?['image'] as Map<String, dynamic>?),
          isFavorite: (map?['isFavorite'] as int?) == 1,
          genres: (map?['genres'] as List?)?.cast() ??
              (map?['allGenres'] as String?)?.split(', ') ??
              [],
          name: map?['name'] as String? ?? '',
          schedule: Schedule.fromMap(map?['schedule'] as Map<String, dynamic>?),
          scheduleFormatted: map?['scheduleFormatted'] as String?,
          summary: map?['summary'] as String? ?? '',
        );

  String get allGenres {
    return genres.join(', ');
  }

  bool get isFavorite {
    return _isFavorite;
  }

  set isFavorite(value) {
    _isFavorite = value;
    if (value) {
      ShowRepository().save(this);
    } else {
      ShowRepository().delete(this);
    }
  }

  String get scheduleFormatted {
    return _scheduleFormatted ??= schedule.time.isEmpty
        ? schedule.days.join(', ')
        : schedule.days.isNotEmpty
            ? '${schedule.days.join(', ')} at ${schedule.time}'
            : schedule.time;
  }

  set scheduleFormatted(value) {
    _scheduleFormatted = value;
  }

  Future<void> _getFavoriteFromLocal() async {
    final database = await Local().database;
    final item = await Local().showStore.record(id).get(database);
    _isFavorite = item?['isFavorite'] == 1;
  }

  Map<String, dynamic> toMap({bool isLocal = false}) {
    final map = <String, dynamic>{
      'id': id,
      'name': name,
      'summary': summary,
    };

    if (isLocal) {
      map['allGenres'] = allGenres;
      map['imageUrl'] = image.medium;
      map['isFavorite'] = isFavorite ? 1 : 0;
      map['scheduleFormatted'] = scheduleFormatted;
    } else {
      map['image'] = image.toMap();
      map['genres'] = genres;
      map['schedule'] = schedule.toMap();
    }

    return map;
  }
}
