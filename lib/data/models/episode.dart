import 'package:tvmaze/data/models/image_data.dart';

class Episode {
  final int id;
  final int number;
  final int season;
  final ImageData image;
  final String name;
  final String summary;

  const Episode({
    this.id = 0,
    this.image = const ImageData(),
    this.name = '',
    this.number = 0,
    this.season = 0,
    this.summary = '',
  });

  Episode.fromMap(Map<String, dynamic>? map)
      : this(
          id: map?['id'] as int? ?? 0,
          image: ImageData.fromMap(map?['image'] as Map<String, dynamic>?),
          name: map?['name'] as String? ?? '',
          number: map?['number'] as int? ?? 0,
          season: map?['season'] as int? ?? 0,
          summary: map?['summary'] as String? ?? '',
        );

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{
      'id': id,
      'image': image.toMap(),
      'name': name,
      'number': number,
      'season': season,
      'summary': summary,
    };

    return map;
  }
}
