class ImageData {
  final String medium;
  final String original;

  const ImageData({
    this.medium = '',
    this.original = '',
  });

  ImageData.fromMap(Map<String, dynamic>? map)
      : this(
          medium: map?['medium'] as String? ?? '',
          original: map?['original'] as String? ?? '',
        );

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{
      'medium': medium,
      'original': original,
    };

    return map;
  }
}
