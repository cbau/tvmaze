import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:tvmaze/data/local.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/data/remote.dart';

/// Show repository.
class ShowRepository {
  static final _instance = ShowRepository._();
  static final _store = Local().showStore;

  var _current = <Show>[];
  var _items = <Show>[];

  /// Private constructor.
  ShowRepository._();

  /// Factory instance constructor.
  factory ShowRepository() => _instance;

  /// Retrieves all the items from this repository.
  List<Show> get all => _items;

  /// Retrieves the current items from this repository.
  List<Show> get current => _items;

  /// Gets all the favorite items from the local source.
  Future<List<Show>> get favorites async {
    final database = await Local().database;
    final finder = Finder(
      filter: Filter.equals('isFavorite', 1),
      sortOrders: [SortOrder('name')],
    );
    var snapshot = await _store.find(database, finder: finder);
    return snapshot.map((e) => Show.fromMap(e.value)).toList();
  }

  /// Downloads all the items from the remote source.
  Future<void> download({
    int page = 0,
  }) async {
    if (page == 0) {
      _items = [];
    }

    final path = '${Remote().apiShows}?page=$page';
    final response = await Remote().getRemoteString(
      method: 'GET',
      path: path,
    );

    if (response.isNotEmpty) {
      final list = json.decode(response) as List?;
      final items = list
              ?.map((e) =>
                  e == null ? Show() : Show.fromMap(e as Map<String, dynamic>?))
              .toList() ??
          [];
      _items.addAll(items);
    }
  }

  /// Downloads all the items from the remote source for this person.
  Future<void> downloadForPersonId(int personId) async {
    final path =
        '${Remote().apiPeopleCastCredits.replaceFirst(':personId', personId.toString())}?embed=show';
    final response = await Remote().getRemoteString(
      method: 'GET',
      path: path,
    );

    if (response.isNotEmpty) {
      final list = json.decode(response) as List?;
      _current = list
              ?.map((e) => e == null
                  ? Show()
                  : Show.fromMap(((e as Map<String, dynamic>?)?['_embedded']
                          as Map<String, dynamic>?)?['show']
                      as Map<String, dynamic>?))
              .toList() ??
          [];
    }
  }

  /// Searchs all the items from the remote source with this query.
  Future<void> searchWithQuery(String query) async {
    final path = '${Remote().apiSearchShows}?q=$query';
    final response = await Remote().getRemoteString(
      method: 'GET',
      path: path,
    );

    if (response.isNotEmpty) {
      final list = json.decode(response) as List?;
      _current = list
              ?.map((e) => e == null
                  ? Show()
                  : Show.fromMap((e as Map<String, dynamic>?)?['show']
                      as Map<String, dynamic>?))
              .toList() ??
          [];
    }
  }

  /// Deletes the item from the local source.
  Future<void> delete(Show item) async {
    final database = await Local().database;
    await _store.record(item.id).delete(database);
  }

  /// Gets the item by its identifier, or null if none is found.
  Show? get(int id) {
    try {
      return _current.firstWhere((element) => element.id == id);
    } catch (e) {
      try {
        return _items.firstWhere((element) => element.id == id);
      } catch (e) {
        return null;
      }
    }
  }

  /// Saves the item into the local source.
  Future<void> save(Show item) async {
    final database = await Local().database;
    await _store.record(item.id).put(database, item.toMap(isLocal: true));
  }
}
