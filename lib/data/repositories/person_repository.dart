import 'dart:convert';

import 'package:tvmaze/data/models/person.dart';
import 'package:tvmaze/data/remote.dart';

/// Person repository.
class PersonRepository {
  static final _instance = PersonRepository._();

  var _items = <Person>[];

  /// Private constructor.
  PersonRepository._();

  /// Factory instance constructor.
  factory PersonRepository() => _instance;

  /// Retrieves all the items from this repository.
  List<Person> get all => _items;

  /// Searchs all the items from the remote source.
  Future<void> searchWithQuery(String query) async {
    final path = '${Remote().apiSearchPeople}?q=$query';
    final response = await Remote().getRemoteString(
      method: 'GET',
      path: path,
    );

    if (response.isNotEmpty) {
      final list = json.decode(response) as List?;
      _items = list
              ?.map((e) => e == null
                  ? const Person()
                  : Person.fromMap((e as Map<String, dynamic>?)?['person']
                      as Map<String, dynamic>?))
              .toList() ??
          [];
    }
  }

  /// Gets the item by its identifier, or null if none is found.
  Person? get(int id) {
    try {
      return _items.firstWhere((element) => element.id == id);
    } catch (e) {
      return null;
    }
  }
}
