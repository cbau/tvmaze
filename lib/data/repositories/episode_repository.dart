import 'dart:convert';

import 'package:tvmaze/data/models/episode.dart';
import 'package:tvmaze/data/remote.dart';

/// Episode repository.
class EpisodeRepository {
  static final _instance = EpisodeRepository._();

  var _items = <Episode>[];

  /// Private constructor.
  EpisodeRepository._();

  /// Factory instance constructor.
  factory EpisodeRepository() => _instance;

  /// Retrieves all the items from this repository.
  List<Episode> get all => _items;

  /// Downloads all the items from the remote source.
  Future<void> download({
    required int showId,
  }) async {
    final path =
        Remote().apiShowEpisodes.replaceFirst(':showId', showId.toString());
    final response = await Remote().getRemoteString(
      method: 'GET',
      path: path,
    );

    if (response.isNotEmpty) {
      final list = json.decode(response) as List?;
      _items = list
              ?.map((e) => e == null
                  ? const Episode()
                  : Episode.fromMap(e as Map<String, dynamic>?))
              .toList() ??
          [];
    }
  }

  /// Gets the item by its identifier, or null if none is found.
  Episode? get(int id) {
    try {
      return _items.firstWhere((element) => element.id == id);
    } catch (e) {
      return null;
    }
  }
}
