import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tvmaze/pages/episode_details_page.dart';
import 'package:tvmaze/pages/favorite_list_page.dart';
import 'package:tvmaze/pages/navigation_page.dart';
import 'package:tvmaze/pages/not_found_page.dart';
import 'package:tvmaze/pages/person_details_page.dart';
import 'package:tvmaze/pages/person_list_page.dart';
import 'package:tvmaze/pages/show_details_page.dart';
import 'package:tvmaze/pages/show_list_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      darkTheme: _themeData(isLight: false),
      //initialRoute: ShowListPage.route,
      onGenerateRoute: (settings) => MaterialPageRoute(
        builder: (context) {
          if (settings.name != null) {
            // EpisodeDetailsPage
            var matches =
                RegExp(EpisodeDetailsPage.route.replaceFirst(':id', r'(\d+)'))
                    .allMatches(settings.name!)
                    .toList();

            if (matches.isNotEmpty) {
              return EpisodeDetailsPage.fromId(matches[0].group(1)) ??
                  const NotFoundPage();
            }

            // PersonDetailsPage
            matches =
                RegExp(PersonDetailsPage.route.replaceFirst(':id', r'(\d+)'))
                    .allMatches(settings.name!)
                    .toList();

            if (matches.isNotEmpty) {
              return PersonDetailsPage.fromId(matches[0].group(1)) ??
                  const NotFoundPage();
            }

            // ShowDetailsPage
            matches =
                RegExp(ShowDetailsPage.route.replaceFirst(':id', r'(\d+)'))
                    .allMatches(settings.name!)
                    .toList();

            if (matches.isNotEmpty) {
              final id = matches[0].group(1);
              return ShowDetailsPage.fromId(id) ?? const NotFoundPage();
            }
          }

          switch (settings.name) {
            case FavoriteListPage.route:
              return const NavigationPage(
                selectedIndex: 2,
              );
            case PersonListPage.route:
              return const NavigationPage(
                selectedIndex: 1,
              );
            case NavigationPage.route:
            case ShowListPage.route:
              return const NavigationPage();
          }

          return const NotFoundPage();
        },
        settings: settings,
      ),
      theme: _themeData(isLight: true),
      //themeMode: ThemeMode.dark,
      title: 'TVmaze Challenge',
    );
  }

  ThemeData _themeData({
    required bool isLight,
  }) {
    return ThemeData(
      brightness: isLight ? Brightness.light : Brightness.dark,
      primarySwatch: Colors.teal,
    );
  }
}
