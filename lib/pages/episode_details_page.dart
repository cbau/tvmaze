import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:simple_html_css/simple_html_css.dart';
import 'package:tvmaze/data/models/episode.dart';
import 'package:tvmaze/data/repositories/episode_repository.dart';

class EpisodeDetailsPage extends StatelessWidget {
  static const route = '/episodes/:id';

  final Episode item;

  const EpisodeDetailsPage({required this.item, Key? key}) : super(key: key);

  static EpisodeDetailsPage? fromId(String? id) {
    if (id != null) {
      final intId = int.parse(id);
      final item = EpisodeRepository().get(intId);
      if (item != null) {
        return EpisodeDetailsPage(item: item);
      }
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            flexibleSpace: FlexibleSpaceBar(
              background: item.image.medium.isEmpty
                  ? null
                  : CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: item.image.medium,
                      width: MediaQuery.of(context).size.width,
                    ),
              title: Text(item.name),
            ),
            expandedHeight: item.image.medium.isEmpty ? null : 200,
            pinned: true,
          ),
          SliverFillRemaining(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Season ${item.season}, episode ${item.number}',
                    style: Theme.of(context).textTheme.caption,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  RichText(
                    text: HTML.toTextSpan(
                      context,
                      item.summary,
                      defaultTextStyle: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
