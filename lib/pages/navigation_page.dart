import 'package:flutter/material.dart';
import 'package:tvmaze/pages/favorite_list_page.dart';
import 'package:tvmaze/pages/person_list_page.dart';
import 'package:tvmaze/pages/show_list_page.dart';

class NavigationPage extends StatefulWidget {
  static const route = '/';

  final int selectedIndex;

  const NavigationPage({
    this.selectedIndex = 0,
    Key? key,
  }) : super(key: key);

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<NavigationPage> {
  var _selectedIndex = 0;
  final _widgetOptions = <Widget>[
    const ShowListPage(),
    const PersonListPage(),
    const FavoriteListPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        //selectedItemColor: AppColors.of(context).primary,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.tv),
            label: 'Shows',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'People',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favorites',
          ),
        ],
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    /*
    final routeName = index == 1
        ? PersonListPage.route
        : index == 2
            ? FavoriteListPage.route
            : ShowListPage.route;
    Navigator.of(context).pushReplacementNamed(routeName);
    */
  }
}
