import 'package:flutter/material.dart';
import 'package:tvmaze/delegates/person_search_delegate.dart';

class PersonListPage extends StatefulWidget {
  static const route = '/people';

  const PersonListPage({Key? key}) : super(key: key);

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<PersonListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: const Icon(Icons.search),
              onPressed: () => showSearch(
                    context: context,
                    delegate: PersonSearchDelegate(),
                  )),
        ],
        title: const Text('People'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text("You haven't searched for anyone yet"),
            ElevatedButton.icon(
              icon: const Icon(Icons.search),
              label: const Text('Search people'),
              onPressed: () => showSearch(
                context: context,
                delegate: PersonSearchDelegate(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
