import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/data/repositories/show_repository.dart';
import 'package:tvmaze/delegates/show_search_delegate.dart';
import 'package:tvmaze/items/show_item.dart';

class ShowListPage extends StatefulWidget {
  static const route = '/shows';

  const ShowListPage({Key? key}) : super(key: key);

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<ShowListPage> {
  final _hasMoreData = true;
  final _itemsPerPage = 250;
  var _currentPage = 0;
  var _isLoading = false;
  var _items = <Show>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: ShowSearchDelegate(),
              );
            },
          ),
        ],
        title: const Text('Shows'),
      ),
      body: IndexedStack(
        index: _items.isNotEmpty
            ? 2
            : _isLoading
                ? 0
                : 1,
        children: [
          const Center(
            child: CircularProgressIndicator(),
          ),
          const Center(
            child: Text('No shows available'),
          ),
          GridView.builder(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              childAspectRatio: 0.6,
              crossAxisSpacing: 8,
              mainAxisSpacing: 8,
              maxCrossAxisExtent: 120,
            ),
            itemCount: _items.isNotEmpty
                ? _items.length + (_hasMoreData ? _itemsPerPage : 0)
                : 0,
            itemBuilder: _showItemBuilder,
            padding: const EdgeInsets.all(8),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    if (_items.isEmpty) {
      _getRemoteData();
    }
  }

  Future<void> _getRemoteData() async {
    _currentPage++;
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        _isLoading = true;
      });
    });

    try {
      await ShowRepository().download(page: _currentPage - 1);
      _items = ShowRepository().all;
    } catch (e) {
      _currentPage--;
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(e.toString()),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          });
    }

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  Widget _showItemBuilder(BuildContext context, int index) {
    if (index >= _currentPage * _itemsPerPage) {
      _getRemoteData();
    }

    if (index >= _items.length) {
      return const Card(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return ShowItem(item: _items[index]);
  }
}
