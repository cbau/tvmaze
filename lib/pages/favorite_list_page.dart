import 'package:flutter/material.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/data/repositories/show_repository.dart';
import 'package:tvmaze/items/show_item.dart';

class FavoriteListPage extends StatefulWidget {
  static const route = '/favorites';

  const FavoriteListPage({Key? key}) : super(key: key);

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<FavoriteListPage> {
  bool _isLoading = false;
  List<Show> _items = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Favorites'),
      ),
      body: IndexedStack(
        index: _items.isNotEmpty
            ? 2
            : _isLoading
                ? 0
                : 1,
        children: [
          const Center(
            child: CircularProgressIndicator(),
          ),
          const Center(
            child: Text(
              'Your favorite shows will appear here.\nGo and show them some love!',
              textAlign: TextAlign.center,
            ),
          ),
          GridView.builder(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              childAspectRatio: 0.6,
              crossAxisSpacing: 8,
              mainAxisSpacing: 8,
              maxCrossAxisExtent: 120,
            ),
            itemCount: _items.length,
            itemBuilder: _showItemBuilder,
            padding: const EdgeInsets.all(8),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    if (_items.isEmpty) {
      _getLocalFavorites();
    }
  }

  Future<void> _getLocalFavorites() async {
    setState(() {
      _isLoading = true;
    });

    _items = await ShowRepository().favorites;

    setState(() {
      _isLoading = false;
    });
  }

  Widget _showItemBuilder(BuildContext context, int index) {
    return ShowItem(item: _items[index]);
  }
}
