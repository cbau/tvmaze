import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tvmaze/data/models/person.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/data/repositories/person_repository.dart';
import 'package:tvmaze/data/repositories/show_repository.dart';
import 'package:tvmaze/items/show_item.dart';

class PersonDetailsPage extends StatefulWidget {
  static const route = '/people/:id';

  final Person item;

  const PersonDetailsPage({required this.item, Key? key}) : super(key: key);

  static PersonDetailsPage? fromId(String? id) {
    if (id != null) {
      final intId = int.parse(id);
      final item = PersonRepository().get(intId);
      if (item != null) {
        return PersonDetailsPage(item: item);
      }
    }

    return null;
  }

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<PersonDetailsPage> {
  bool _isLoading = false;
  List<Show> _shows = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        controller: ScrollController(
          initialScrollOffset: 300,
        ),
        slivers: [
          SliverAppBar(
            flexibleSpace: FlexibleSpaceBar(
              background: widget.item.image.medium.isEmpty
                  ? null
                  : CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: widget.item.image.medium,
                      width: MediaQuery.of(context).size.width,
                    ),
              title: Text(widget.item.name),
            ),
            expandedHeight: widget.item.image.medium.isEmpty ? null : 480,
            pinned: true,
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Shows',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  if (_isLoading)
                    const Center(
                      child: CircularProgressIndicator(),
                    )
                  else if (_shows.isEmpty)
                    const Center(
                      child: Text('No shows available for this person'),
                    )
                ],
              ),
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.all(8),
            sliver: SliverGrid(
              delegate: SliverChildBuilderDelegate(
                _showItemBuilder,
                childCount: _shows.length,
              ),
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                childAspectRatio: 0.6,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
                maxCrossAxisExtent: 120,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getRemoteData();
  }

  Future<void> _getRemoteData() async {
    setState(() {
      _isLoading = true;
    });

    try {
      await ShowRepository().downloadForPersonId(widget.item.id);
      _shows = ShowRepository().current;
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(e.toString()),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          });
    }

    setState(() {
      _isLoading = false;
    });
  }

  Widget _showItemBuilder(BuildContext context, int index) {
    return ShowItem(item: _shows[index]);
  }
}
