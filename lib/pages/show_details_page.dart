import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:simple_html_css/simple_html_css.dart';
import 'package:tvmaze/data/models/episode.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/data/repositories/episode_repository.dart';
import 'package:tvmaze/data/repositories/show_repository.dart';
import 'package:tvmaze/items/episode_item.dart';

class ShowDetailsPage extends StatefulWidget {
  static const route = '/shows/:id';

  final Show item;

  const ShowDetailsPage({required this.item, Key? key}) : super(key: key);

  static ShowDetailsPage? fromId(String? id) {
    if (id != null) {
      final intId = int.parse(id);
      final item = ShowRepository().get(intId);
      if (item != null) {
        return ShowDetailsPage(item: item);
      }
    }

    return null;
  }

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<ShowDetailsPage> {
  bool _isLoading = false;
  List<Episode> _episodes = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        controller: ScrollController(
          initialScrollOffset: 300,
        ),
        slivers: [
          SliverAppBar(
            /*
            actions: [
              IconButton(
                color: Colors.red,
                icon: Icon(widget.item.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border),
                onPressed: () => setState(() {
                  widget.item.isFavorite = !widget.item.isFavorite;
                }),
              ),
            ],
            */
            expandedHeight: widget.item.image.medium.isEmpty ? null : 480,
            flexibleSpace: FlexibleSpaceBar(
              background: widget.item.image.medium.isEmpty
                  ? null
                  : CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: widget.item.image.medium,
                      width: MediaQuery.of(context).size.width,
                    ),
              title: Text(widget.item.name),
            ),
            pinned: true,
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.item.scheduleFormatted,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    'Genre: ${widget.item.allGenres}',
                    style: Theme.of(context).textTheme.caption,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  RichText(
                    text: HTML.toTextSpan(
                      context,
                      widget.item.summary,
                      defaultTextStyle: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              _episodeItemBuilder,
              childCount: _episodes.isNotEmpty ? _episodes.length : 1,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getRemoteData();
  }

  Future<void> _getRemoteData() async {
    setState(() {
      _isLoading = true;
    });

    try {
      await EpisodeRepository().download(showId: widget.item.id);
      _episodes = EpisodeRepository().all;
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(e.toString()),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          });
    }

    setState(() {
      _isLoading = false;
    });
  }

  Widget _episodeItemBuilder(BuildContext context, int index) {
    if (_isLoading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    } else if (index >= _episodes.length) {
      return const Center(
        child: Text('No episodes available for this show'),
      );
    }

    final item = _episodes[index];
    final episodeItem = EpisodeItem(item: item);

    return index == 0 || _episodes[index - 1].season != item.season
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  'Season ${item.season}',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              episodeItem,
            ],
          )
        : episodeItem;
  }
}
