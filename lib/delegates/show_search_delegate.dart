import 'package:flutter/material.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/data/repositories/show_repository.dart';
import 'package:tvmaze/items/show_item.dart';

class ShowSearchDelegate extends SearchDelegate {
  var _items = <Show>[];
  var _lastQuery = '';

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () => query = '',
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return const BackButton();
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder(
      builder: _showFutureBuilder,
      future: _searchRemote(context),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }

  Future<void> _searchRemote(BuildContext context) async {
    if (query == _lastQuery) {
      return;
    } else if (query.isEmpty) {
      _items = [];
      return;
    }

    _lastQuery = query;
    try {
      await ShowRepository().searchWithQuery(query);
      _items = ShowRepository().current;
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(e.toString()),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          });
    }
  }

  Widget _showFutureBuilder(BuildContext context, AsyncSnapshot snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (_items.isNotEmpty) {
        return GridView.builder(
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            childAspectRatio: 0.6,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8,
            maxCrossAxisExtent: 120,
          ),
          itemCount: _items.length,
          itemBuilder: _showItemBuilder,
          padding: const EdgeInsets.all(8),
        );
      } else if (query.isNotEmpty) {
        return Center(
          child: Text('Result not found for $query'),
        );
      }

      return Container();
    }

    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _showItemBuilder(BuildContext context, int index) {
    return ShowItem(item: _items[index]);
  }
}
