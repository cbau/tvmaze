import 'package:flutter/material.dart';
import 'package:tvmaze/data/models/person.dart';
import 'package:tvmaze/data/repositories/person_repository.dart';
import 'package:tvmaze/items/person_item.dart';

class PersonSearchDelegate extends SearchDelegate {
  var _items = <Person>[];
  var _lastQuery = '';

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () => query = '',
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return const BackButton();
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder(
      builder: _personFutureBuilder,
      future: _searchRemote(context),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }

  Future<void> _searchRemote(BuildContext context) async {
    if (query == _lastQuery) {
      return;
    } else if (query.isEmpty) {
      _items = [];
      return;
    }

    _lastQuery = query;

    try {
      await PersonRepository().searchWithQuery(query);
      _items = PersonRepository().all;
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(e.toString()),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          });
    }
  }

  Widget _personFutureBuilder(BuildContext context, AsyncSnapshot snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (_items.isNotEmpty) {
        return GridView.builder(
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            childAspectRatio: 0.6,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8,
            maxCrossAxisExtent: 120,
          ),
          itemCount: _items.length,
          itemBuilder: _personItemBuilder,
          padding: const EdgeInsets.all(8),
        );
      } else if (query.isNotEmpty) {
        return Center(
          child: Text('Result not found for $query'),
        );
      }

      return Container();
    }

    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _personItemBuilder(BuildContext context, int index) {
    return PersonItem(item: _items[index]);
  }
}
