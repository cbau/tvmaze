import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tvmaze/data/models/show.dart';
import 'package:tvmaze/pages/show_details_page.dart';

class ShowItem extends StatefulWidget {
  final Show item;

  const ShowItem({required this.item, Key? key}) : super(key: key);

  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<ShowItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).primaryColor,
      child: InkWell(
          onTap: () => Navigator.of(context).pushNamed(
                ShowDetailsPage.route.replaceFirst(
                  ':id',
                  widget.item.id.toString(),
                ),
              ),
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              Column(
                children: [
                  Expanded(
                    child: widget.item.image.medium.isEmpty
                        ? Container()
                        : CachedNetworkImage(
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.broken_image),
                            fit: BoxFit.cover,
                            imageUrl: widget.item.image.medium,
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) => Center(
                              child: CircularProgressIndicator(
                                  value: downloadProgress.progress),
                            ),
                            width: MediaQuery.of(context).size.width,
                          ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4),
                    child: Text(
                      widget.item.name + '\n',
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).primaryTextTheme.bodyText2,
                    ),
                  ),
                ],
              ),
              IconButton(
                color: Colors.red,
                icon: Icon(widget.item.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border),
                onPressed: () => setState(() {
                  widget.item.isFavorite = !widget.item.isFavorite;
                }),
              ),
            ],
          )),
    );
  }
}
