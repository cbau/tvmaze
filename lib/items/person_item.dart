import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tvmaze/data/models/person.dart';
import 'package:tvmaze/pages/person_details_page.dart';

class PersonItem extends StatelessWidget {
  final Person item;

  const PersonItem({required this.item, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).primaryColor,
      child: InkWell(
        onTap: () => Navigator.of(context).pushNamed(
          PersonDetailsPage.route.replaceFirst(
            ':id',
            item.id.toString(),
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: item.image.medium.isEmpty
                  ? Container()
                  : CachedNetworkImage(
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.broken_image),
                      fit: BoxFit.cover,
                      imageUrl: item.image.medium,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) => Center(
                        child: CircularProgressIndicator(
                            value: downloadProgress.progress),
                      ),
                      width: MediaQuery.of(context).size.width,
                    ),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Text(
                item.name + '\n',
                maxLines: 2,
                textAlign: TextAlign.center,
                style: Theme.of(context).primaryTextTheme.bodyText2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
