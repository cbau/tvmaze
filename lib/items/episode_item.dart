import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:simple_html_css/simple_html_css.dart';
import 'package:tvmaze/data/models/episode.dart';
import 'package:tvmaze/pages/episode_details_page.dart';

class EpisodeItem extends StatelessWidget {
  final Episode item;

  const EpisodeItem({required this.item, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 1),
      child: ListTile(
        leading: SizedBox(
          width: 80,
          child: item.image.medium.isEmpty
              ? null
              : CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl: item.image.medium,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      Center(
                    child: CircularProgressIndicator(
                        value: downloadProgress.progress),
                  ),
                  width: 80,
                ),
        ),
        onTap: () => Navigator.of(context).pushNamed(
          EpisodeDetailsPage.route.replaceFirst(
            ':id',
            item.id.toString(),
          ),
        ),
        subtitle: RichText(
          text: HTML.toTextSpan(
            context,
            item.summary,
            defaultTextStyle: Theme.of(context).textTheme.bodyText2,
          ),
          overflow: TextOverflow.ellipsis,
        ),
        tileColor: Theme.of(context).backgroundColor,
        title: Text('${item.number}. ${item.name}'),
      ),
    );
  }
}
