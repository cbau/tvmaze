# TVmaze Challenge

Flutter challenge consuming the api from TVmaze.

## Features

The challenge contains the next features:

- View a list with all the shows.
- Load new shows dynamically through an infinite scroll.
- View the show details and chapters.
- View the chapters details.
- Search any show by name.
- Search people by name.
- Mark and see your favorite shows.

Version 1.0.1 also contains the next fixes:

- Compatibility with dark mode.
- Order your favorite shows by name.
- Crash when connection is lost.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/tvmaze/)

To download the latest version, visit the releases page:

[Releases](https://gitlab.com/cbau/tvmaze/-/releases)
